<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExpandCommentsTable extends Migration
{
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->datetime('due_at')->nullable();
            $table->datetime('completed_at')->nullable();
            $table->bigInteger('created_for')->unsigned()->nullable();
            $table->foreign('created_for')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('comments');
    }
}
