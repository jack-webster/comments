<?php

namespace Jackwebs\Comments;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Michelf\Markdown;

class Comment extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    protected static function boot()
    {
        parent::boot();
        /*
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('updated_at', 'desc');
        });
        */
    }

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'commentable_id',
        'commentable_type',
        'text',
        'created_by',
        'due_at',
        'completed_at',
        'created_for'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'due_at',
        'created_at',
        'completed_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the creator for this model.
     *
     * @return App\User
     */
    public function creator()
    {
        return $this->belongsTo(
            config('auth.providers.users.model'),
            'created_by'
        );
    }

    public function recipient()
    {
        return $this->belongsTo(
            config('auth.providers.users.model'),
            'created_for'
        );
    }

    public function commentable(): MorphTo
    {
        return $this->morphTo();
    }

    public function getRawTextAttribute($value)
    {
        return $this->attributes['text'];
    }

    public function getPlainTextAttribute($value)
    {
        return strip_tags($this->text);
    }

    public function getTextAttribute($value)
    {
        return Markdown::defaultTransform($value);
    }

    public function scopeOverdue($query)
    {
        return $query->tasks()->where('due_at', '<', now());
    }

    public function scopePending($query)
    {
        return $query->whereNotNull('created_for')->whereNull('completed_at');
    }

    public function scopeCompleted($query)
    {
        return $query->whereNotNull('created_for')->whereNotNull('completed_at');
    }
}
