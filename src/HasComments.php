<?php

namespace Jackwebs\Comments;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Collection;

trait HasComments
{
    public static function getCommentClassName(): string
    {
        return Comment::class;
    }

    public function tasks()
    {
        return $this->allComments()->withoutGlobalScope('order')->pending()->orderBy('due_at');
    }

    public function allComments(): MorphMany
    {
        $class = self::getCommentClassName();
        return $this
            ->morphMany($class, 'commentable');
    }

    public function comments()
    {
        return $this->allComments()->whereNull('due_at')->orderBy('updated_at', 'desc');
    }
}
